import { UIInput, OnBlurEvent, OnChangeEvent, Param, convertorValue, UIMessageHelper } from 'rainbowui-desktop-core';
import config from "config";
import PropTypes from 'prop-types';
import { StringUtil, Util, r18n } from 'rainbow-desktop-tools';
import { ValidatorContext, SessionContext } from 'rainbow-desktop-cache';
import '../js/bootstrap-datetimepicker';
import '../css/bootstrap-datetimepicker.css';

export default class DateTimePicker extends UIInput {

    constructor(props) {
        super(props);
    }

    renderInput() {

        return (
            <div class="input-group">
                <input type="text" autocomplete="off" className="form-control" maxlength={this.props.format.length} title={this.getI18n(this.props.title)} style={this.props.style}
                    id={this.componentId} name={this.getName()} placeholder={this.props.placeholder ? this.props.placeholder : ""} data-auto-test={this.getNameForTest()} />
                {this.renderDeleteIcon()}
                <span className={this.getDisabled() ? "input-group-addon pickerposition disabled" : "input-group-addon pickerposition"}>
                    <span class="rainbow Calendar"
                        onClick={this.onClickCanlder} />
                </span>
            </div>
        );
    }
    renderDeleteIcon() {
        return (
            <span class="rainbow Clear deleteIcon" id={this.componentId + "_deleteIcon"}
                style={{ display: 'none' }}></span>
        )
    }
    renderOutput() {
        const outPutClass = "outPutText";
        let value = this.props.model && this.props.property ? this.props.model[this.props.property] : this.props.value ? this.props.value : '';
        let output = this.getAsString(value);
        if (Util.parseBool(this.props.isShowValueTooltip)) {
            return (
                <span id={this.componentId} className={outPutClass} data-toggle="tooltip" data-placement="bottom" data-original-title={output} style={this.props.style}>
                    {output}
                </span>
            );
        } else {
            return (
                <span id={this.componentId} className={outPutClass} style={this.props.style}>
                    {output}
                </span>
            );
        }
    }


    componentDidUpdate(nextProps, nextState) {
        super.componentDidUpdate(nextProps, nextState);
        if (!this.isDynamicProduct() && this.props.io != 'out') {
            // this.clearValidationInfo(nextProps);
            this.initComponent();
            let _self = this;
            let newInputValue = _self.getInputValue(event);
            _self.onEvent = { newValue: newInputValue, oldValue: _self.onEvent.newValue };
            $("#" + this.componentId).datetimepicker('update');
        }
    }



    initComponent() {
        if (this.props.io == 'out') return;
        const dateComponent = $("#" + this.componentId);
        let _self = this;
        if (this.props.property && this.props.model != null && this.props.model[this.props.property]) {
            // dateComponent.datetimepicker('update');
            this.conversorInput(this.props.model[this.props.property])
        }

        dateComponent.datetimepicker({
            format: this.getFormat(),//显示格式
            startView: this.getStartView(), //日期时间选择器所能够提供的最精确的时间选择视图
            minView: this.getMinView(),   //日期时间选择器最高能展示的选择范围视图
            startDate: this.getMinDate(),   //  日期视图可选择的开始日期
            endDate: this.getMaxDate(),   //  日期视图可选择的截止日期
            language: this.getLanguage(),
            autoclose: 1, //选择后自动关闭
            clearBtn: Util.parseBool(this.props.showClear),//清除按钮
            weekStart: 0, //一周从哪一天开始。0（星期日）到6（星期六）
            todayBtn: Util.parseBool(this.props.showToday),//"Today" 按钮
            forceParse: false,
            minuteStep: 1,
            fontAwesome: '', // 图标前缀
            pickerPosition: this.props.pickerPosition,
            // clickCallBack: function (event) {
            //     _self.setInputValue(event, _self, dateComponent);
            // },
            confirmCallBack: function (event) {
                _self.setInputValue(event, _self, dateComponent);
            }
        });

        dateComponent.datetimepicker('setStartDate', this.getMinDate());
        dateComponent.datetimepicker('setEndDate', this.getMaxDate());

        dateComponent.unbind("blur");
        dateComponent.blur((event) => {
            $('#' + this.componentId + '_deleteIcon').css({ display: 'none' });
            let val = dateComponent.val()
            let flag = this.formatInputValue(val, this.getFormat())
            if (!flag && val && val !== '') {
                UIMessageHelper.warning(r18n.formatMsg)
                dateComponent.val('')
                dateComponent.datetimepicker('update');
            }
            _self.setComponentValue(event)
            dateComponent.parent().removeClass("focusborder");
            _self.props.onBlur ? _self.props.onBlur(new OnBlurEvent(_self, event, Param.getParameter(_self), null, null)) : null;
        });
        dateComponent.click(function () {
            dateComponent.datetimepicker('show');
        });
    }

    // 点击日期图标，显示日期选择控件
    onClickCanlder = () => {
        if (!this.getDisabled()) {
            const dateComponent = $("#" + this.componentId);
            dateComponent.datetimepicker('show');
        }

    }
    // 设置日期视图开始是年，月还是日
    /*
        5: 十年视图
        4：年视图，可显示月
        3：月视图，可显示日
    */
    getStartView() {
        let format = this.getFormat();
        if (format === "yyyy") {
            return 5;
        } else if (format.indexOf("dd") === -1) {
            return 4;
        } else {
            return 3;
        }
    }
    // 设置可选择的最小日期
    getMinDate() {
        let format = config.DEFAULT_DATETIME_SUBMIT_FORMATER ? config.DEFAULT_DATETIME_SUBMIT_FORMATER : 'YYYY-MM-DDTHH:mm:ss';
        let { minDate } = this.props;
        if (!minDate) {
            minDate = "0001-01-01T00:00:00";
        }
        if (minDate && Object.prototype.toString.call(minDate) == "[object String]" && minDate.toUpperCase() === "TODAY") {
            const today = window.ServerDate ? new ServerDate() : new Date();
            let minToday = dayjs(today).format("YYYY-MM-DDT00:00:00");
            return minToday;
        } else {
            return dayjs(minDate).format(format);
        }
    }
    // 设置可选择的最大日期
    getMaxDate() {
        let format = config.DEFAULT_DATETIME_SUBMIT_FORMATER ? config.DEFAULT_DATETIME_SUBMIT_FORMATER : 'YYYY-MM-DDTHH:mm:ss';
        let { maxDate } = this.props;
        if (!maxDate) {
            maxDate = "9999-12-31T23:59:59";
        }
        if (maxDate && Object.prototype.toString.call(maxDate) == "[object String]" && maxDate.toUpperCase() === "TODAY") {
            const today = window.ServerDate ? new ServerDate() : new Date();
            let maxToday = dayjs(today).format("YYYY-MM-DDT23:59:59");
            return maxToday;
        } else {
            return dayjs(maxDate).format(format);
        }
    }

    getFormat() {
        let format = this.props.format;
        if (format.indexOf("mm") >= 0) {
            format = format.replace("mm", "ii");
        };
        if (format.indexOf("YYYY") >= 0) {
            format = format.replace("YYYY", "yyyy");
        }
        if (format.indexOf("MM") >= 0) {
            format = format.replace("MM", "mm");
        }
        if (format.indexOf("DD") >= 0) {
            format = format.replace("DD", "dd");
        };
        if (format.indexOf("HH") >= 0) {
            format = format.replace("HH", "hh");
        } else if (format.indexOf("hh") >= 0) {
            format = format.replace("hh", "HH");
        };
        return format;
    }

    dayjsFormat() {
        let format = this.props.format;
        if (format.indexOf("ii") >= 0) {
            format = format.replace("ii", "mm");
        };
        return format;
    }

    getLanguage() {
        let localLang = sessionStorage.getItem('system_i18nKey') ? sessionStorage.getItem('system_i18nKey') : "en_US";
        let lang;
        switch (localLang) {
            case "pt":
                lang = "pt";
                break;
            case "zh_CN":
                lang = "zh";
                break;
            default:
                lang = "en";
        }
        return lang;
    }

    getMinView() {
        let minView = 0;
        let format = this.getFormat();
        if (format === "yyyy") {
            minView = 5;
        } else if (format.indexOf("dd") === -1) {
            minView = 4;
        } else if (format.endsWith("yyyy")) {
            minView = 3;
        } else if (format.endsWith("HH") || format.endsWith("hh")) {
            minView = 2;
        } else if (format.endsWith("ii")) {
            minView = 1;
        }
        return minView;
    }

    isShowTime(format) {
        let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        let showTime = false;
        if (tempFormat.indexOf("hh") > -1 || tempFormat.indexOf("HH") > -1 || tempFormat.indexOf("ii") > -1 || tempFormat.indexOf("ss") > -1) {
            showTime = true;
        }
        return showTime;
    }

    setComponentValue(event) {
        let inputValue = this.getInputValue(event);
        if (!dayjs(inputValue).isValid()) {
            inputValue = null;
        }
        this.setValue(this.props.value, inputValue);
    }

    onChangeCallback(_self) {
        let inputValue = this.getInputValue(event);
        this.setComponentValue(event);
        if (this.props.onChange) {
            let valueChangeEvent = new OnChangeEvent(this, event, Param.getParameter(this), inputValue, this.onEvent.newValue);
            this.props.model[this.props.property] = inputValue;
            this.props.onChange(valueChangeEvent);
        }
        this.onEvent = { newValue: inputValue, oldValue: this.onEvent.newValue };
    }

    // formatInputValue(val,format){ //手动输入 还是要按照format格式来输入
    //     let newFormat = format.split(this.props.formatDateSplit)
    //     let dateFormat = ''
    //     let str = ''
    //     let flag =true
    //     let showTime = this.isShowTime(format);
    //     let valList=val.split(' ')[0].split(this.props.formatDateSplit)
    //     if(newFormat[0]=='yyyy'){
    //         if(val.slice(4, 5)!==this.props.formatDateSplit||val.slice(7, 8)!==this.props.formatDateSplit||valList[0].length!==4){
    //             flag =false
    //         }
    //     }else{
    //         if(val.slice(2, 3)!==this.props.formatDateSplit||val.slice(5, 6)!==this.props.formatDateSplit||valList[2].length!==4){
    //             flag =false
    //         }
    //     }
    //     if(showTime){
    //         if(val.slice(13, 14)!==':'){
    //             flag =false
    //         }
    //     }
    //     return flag
    // }

    formatInputValue(val, format) { //手动输入 还是要按照format格式来输入
        let flag = false;
        let tempFormat = format.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        let tempVal = val.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
        if (tempFormat.length == tempVal.length) {
            flag = true;
        }
        return flag
    }

    setInputValue(event, _self, dateComponent) {
        _self.setComponentValue();
        let value = _self.getInputValue(event);
        if (this.props.onChange) {
            // this.props.onChange(_self);
            this.onChangeCallback(_self);
        }
        if (Util.parseBool(_self.props.required)) {
            const helpObject = $("#for-input-" + this.componentId).next(".help-block");
            if (value && value != "0") {
                const inputObject = $("#" + this.componentId);
                const errorInputGroupObject = inputObject.closest(".form-group").parent().parent().parent().parent().parent();
                const errorInputObject = inputObject.closest(".form-group");
                if (errorInputObject.hasClass("has-error")) {
                    inputObject.parent().next().remove();
                    errorInputObject.removeClass("has-feedback has-error").addClass("has-success");
                    helpObject.hide();

                }
                if (errorInputGroupObject.hasClass("has-error")) {
                    inputObject.parent().parent().parent().parent().parent().parent().next().remove();
                    inputObject.parent().parent().parent().parent().parent().parent().next().remove();
                    errorInputGroupObject.removeClass("has-feedback has-error").addClass("has-success");
                    helpObject.hide();
                }
                dateComponent.parent().removeClass("input-required");
            } else {
                const inputObject = $("#" + this.componentId);
                const errorInputObject = inputObject.closest(".form-group");
                const errorInputGroupObject = inputObject.closest(".form-group").parent().parent().parent().parent().parent();
                if (!_.isEmpty(helpObject)) {
                    errorInputObject.addClass("has-feedback has-error");
                    errorInputGroupObject.addClass("has-feedback has-error");
                    helpObject.show();
                }
                dateComponent.parent().addClass("input-required");
            }
        }
        const findClass = this.props.findClass ? "." + this.props.findClass : ".input-group";
        dateComponent.closest(findClass).removeClass("focusborder");
    }
    getInputValue(event) {
        const dateComponent = $("#" + this.componentId);
        const today = window.ServerDate ? new ServerDate() : new Date();
        let val = dateComponent.val();
        if (!StringUtil.isEmpty(val)) {
            let showFormat = this.getFormat();
            let tempFormat = showFormat.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            let tempVal = val.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\"|\'|\<|\.|\>|\/|\?|\:|\T]/g, "");
            let showTime = this.isShowTime(showFormat);
            let parseFormat = dateComponent.datetimepicker.DPGlobal.parseFormat(showFormat, "standard");
            let backSplit = config.DEFAULT_DATETIME_SUBMIT_FORMATER.slice(4, 5);
            let year = today.getFullYear();
            let month = today.getMonth();
            let dayMonth = today.getDate();
            let hour = today.getHours();
            let minute = today.getMinutes();
            let second = today.getSeconds();
            if (showTime) {
                if (this.props.formatSplit && this.props.formatDateSplit) {
                    //date
                    let showDateList = val.split(this.props.formatSplit);
                    let showDateDate = showDateList && showDateList.length > 0 ? showDateList[0] : '';
                    let showDateTime = showDateList && showDateList.length > 0 ? showDateList[1] : '';
                    let showDateDateList = showDateDate.split(this.props.formatDateSplit);
                    let showDateTimeList = showDateTime ? showDateTime.split(':') : [];
                    //format
                    let showFormatList = showFormat.split(this.props.formatSplit);
                    let showFormatDate = showFormatList && showFormatList.length > 0 ? showFormatList[0] : '';
                    let showFormatTime = showFormatList && showFormatList.length > 0 ? showFormatList[1] : '';
                    let showFormatDateList = showFormatDate.split(this.props.formatDateSplit);
                    let showFormatTimeList = showFormatTime.split(':');

                    let newDateList = {};
                    for (let i = 0; i < showFormatDateList.length; i++) {
                        newDateList[showFormatDateList[i]] = showDateDateList[i];
                    }
                    year = newDateList.yyyy ? newDateList.yyyy : year;
                    month = newDateList.mm ? newDateList.mm : month;
                    dayMonth = newDateList.dd ? newDateList.dd : dayMonth;

                    let newTimeList = [];
                    for (let i = 0; i < 3; i++) {
                        let time = showDateTimeList && showDateTimeList[i] ? showDateTimeList[i] : '00';
                        newTimeList.push(time);
                    }
                    hour = newTimeList[0];
                    minute = newTimeList[1];
                    second = newTimeList[2];
                    let newDateTime = newTimeList.join(':');

                    let uctDate = new Date(Date.UTC.apply(Date, [year, month > 0 ? month - 1 : 0, dayMonth, hour, minute, second]));
                    let parseDate = dateComponent.datetimepicker.DPGlobal.formatDate(uctDate, parseFormat, this.getLanguage(), "standard");

                    if ((tempFormat.length == tempVal.length) && (parseDate != val)) {
                        val = "";
                        $('#' + this.componentId).val("");
                        UIMessageHelper.warning(r18n.formatMsg);
                    } else {
                        let setDateList = [];
                        let setFormatList = config.DEFAULT_DATETIME_SUBMIT_FORMATER.split(this.props.dateConnector)[0].toLowerCase().split(backSplit);
                        setFormatList.forEach(item => {
                            setDateList.push(newDateList[item])
                        });
                        let newDateDate = setDateList.join(backSplit);
                        val = newDateDate + this.props.dateConnector + newDateTime
                    }
                } else {
                    val = "";
                    $('#' + this.componentId).val("");
                    UIMessageHelper.warning(r18n.formatSplitBothIsEmpty)
                }
            } else {
                if (this.props.formatDateSplit) {
                    //date
                    let showDateList = val.split(this.props.formatSplit);
                    let showDateDate = showDateList && showDateList.length > 0 ? showDateList[0] : '';
                    let showDateDateList = showDateDate.split(this.props.formatDateSplit);
                    //format
                    let showFormatList = showFormat.split(this.props.formatSplit);
                    let showFormatDate = showFormatList && showFormatList.length > 0 ? showFormatList[0] : '';
                    let showFormatDateList = showFormatDate.split(this.props.formatDateSplit);

                    let newDateList = {};
                    for (let i = 0; i < showFormatDateList.length; i++) {
                        newDateList[showFormatDateList[i]] = showDateDateList[i];
                    }
                    year = newDateList.yyyy ? newDateList.yyyy : year;
                    month = newDateList.mm ? newDateList.mm : month;
                    // dayMonth = newDateList.dd?newDateList.dd:dayMonth;
                    // 解决选择二月报错问题，因为当本地时间是30日或31日时，超出二月范围
                    dayMonth = newDateList.dd ? newDateList.dd : "01";

                    let uctDate = new Date(Date.UTC.apply(Date, [year, month > 0 ? month - 1 : 0, dayMonth]));
                    let parseDate = dateComponent.datetimepicker.DPGlobal.formatDate(uctDate, parseFormat, this.getLanguage(), "standard");

                    if ((tempFormat.length == tempVal.length) && (parseDate != val)) {
                        val = "";
                        $('#' + this.componentId).val("");
                        UIMessageHelper.warning(r18n.formatMsg);
                    } else {
                        let setDateList = [];
                        let setFormatList = config.DEFAULT_DATETIME_SUBMIT_FORMATER.split(this.props.dateConnector)[0].toLowerCase().split(backSplit);
                        setFormatList.forEach(item => {
                            if (newDateList[item]) {
                                setDateList.push(newDateList[item])
                            }

                        });
                        let newDateDate = setDateList.join(backSplit);
                        if (showFormat === 'yyyy') {
                            val = newDateDate;
                        } else if (showFormat === 'mm/yyyy') {
                            val = newDateDate + backSplit + "01" + this.props.dateConnector + "00:00:00"
                        } else {
                            val = newDateDate + this.props.dateConnector + "00:00:00";
                        }
                    }
                } else {
                    val = "";
                    $('#' + this.componentId).val("");
                    UIMessageHelper.warning(r18n.formatSplitIsEmpty)
                }
            }
        }
        if (!dayjs(val).isValid()) {
            val = null;
        }
        return val;
    }

    conversorInput(date) {
        let showDate = this.getAsString(date);
        let maxDate = this.getMaxDate();
        let minDate = this.getMinDate();
        if (dayjs(date).isAfter(maxDate) || dayjs(date).isBefore(minDate)) {
            showDate = null;
            this.props.model[this.props.property] = '';
            if (this.props.onChange) {
                let valueChangeEvent = new OnChangeEvent(this, event, Param.getParameter(this), '', this.onEvent.newValue);
                this.props.onChange(valueChangeEvent);
            }
        }
        if (this.props.io == "out") {
            $('#' + this.componentId).text(showDate)
        } else {
            $('#' + this.componentId).val(showDate);
        }
    }

    getAsString(date) {
        if (!date) {
            return ''
        } else {
            if (!dayjs(date).isValid()) {
                return '';
            }
        }
        let dayjsFormat = this.dayjsFormat();
        let showFullDate = dayjs(date).format(dayjsFormat);
        return showFullDate;

        // if (this.props.formatSplit&&this.props.formatDateSplit) {
        //     let dayjsFormat = this.dayjsFormat();
        //     let showFullDate = dayjs(date).format(dayjsFormat);
        //     return showFullDate;
        // }else {
        //     UIMessageHelper.warning(r18n.formatSplitBothIsEmpty);
        //     return '';
        // }

        // let showFormat = this.getFormat();        
        // let showTime = this.isShowTime(showFormat);
        // let dateFullList = date.split(this.props.dateConnector);
        // let dateTime = dateFullList&&dateFullList[1]?dateFullList[1]:'00:00:00';
        // let dateTimeList = dateTime.split(":");
        // let backSplit = config.DEFAULT_DATETIME_SUBMIT_FORMATER.slice(4, 5);
        // let dateList = dateFullList&&dateFullList[0]?dateFullList[0].split(backSplit):[];
        // let dateObj = {};
        // if(dateList&&dateList.length>0){
        //     dateObj.yyyy = dateList[0];
        //     dateObj.mm = dateList[1];
        //     dateObj.dd = dateList[2];
        // }
        // if(showTime){
        //     if(this.props.formatSplit&&this.props.formatDateSplit){
        //         let showFormatList = showFormat.split(this.props.formatSplit);
        //         let showFormatDate = showFormatList&&showFormatList.length>0?showFormatList[0]:'';
        //         let showFormatTime = showFormatList&&showFormatList.length>0?showFormatList[1]:'';
        //         let showFormatDateList = showFormatDate.split(this.props.formatDateSplit);
        //         let showFormatTimeList = showFormatTime.split(':');                
        //         let showDateList = [];
        //         showFormatDateList.forEach(item => {
        //             showDateList.push(dateObj[item])
        //         });
        //         let showDate = showDateList.join(this.props.formatDateSplit);
        //         let newDateTimeList = [];
        //         for(let i=0;i<showFormatTimeList.length;i++){
        //             newDateTimeList.push(dateTimeList[i])
        //         }
        //         let newDateTime = newDateTimeList.join(':');               
        //         let showFullDate = showDate+this.props.formatSplit+newDateTime;
        //         let dayjsFormat = this.dayjsFormat();
        //         //     showFullDate = dayjs(showFullDate).format(dayjsFormat);  // dayjs的问题不显示dd/mm/yyyy
        //         showFullDate = dayjs(date).format(dayjsFormat);  //  dayjs参数问题参数必须为 YYYY/MM/DD的格式
        //         return showFullDate;
        //     }else{
        //         UIMessageHelper.warning(r18n.formatSplitBothIsEmpty);
        //         return '';
        //     }
        // }else{
        //     if(this.props.formatDateSplit){
        //         let showFormatList = showFormat.split(this.props.formatSplit);
        //         let showFormatDate = showFormatList&&showFormatList.length>0?showFormatList[0]:'';
        //         let showFormatDateList = showFormatDate.split(this.props.formatDateSplit);
        //         let showDateList = [];
        //         showFormatDateList.forEach(item => {
        //             showDateList.push(dateObj[item])
        //         });
        //         let showDate = showDateList.join(this.props.formatDateSplit);
        //         return showDate;
        //     }else{
        //         UIMessageHelper.warning(r18n.formatSplitIsEmpty);
        //         return '';
        //     }
        // }
    }

}

/**@ignore
 * DateTimePicker component prop types
 */
DateTimePicker.propTypes = $.extend({}, UIInput.propTypes, {
    minDate: PropTypes.string,
    maxDate: PropTypes.string,
    format: PropTypes.string,
    showToday: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    showClear: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    formatSplit: PropTypes.string,
    formatDateSplit: PropTypes.string,
    dateConnector: PropTypes.string,
    componentType: PropTypes.string
});

/**@ignore
 * Get DateTimePicker component default props
 */
DateTimePicker.defaultProps = $.extend({}, UIInput.defaultProps, {
    showTime: false,
    showToday: true,
    showClear: true,
    format: config.DEFAULT_DATETIME_FORMATER,
    dateConnector: config.DEFAULT_DATE_CONNECTOR || 'T',
    componentType: "newDatetimepicker",
    formatDateSplit: "/",
    formatSplit: " ",
    minDate: "0001-01-01",
    maxDate: "9999-12-31"
});
